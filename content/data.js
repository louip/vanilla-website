console.log("data.js")

const user = {
    name: "louis",
    surname: "perreau",
    age: 22,
    is_premium: true,
    pets: ["dog", "cat"]
}

export const company = {
    name: "Omio",
    location: "Berlin",
    employees: 3439
}

export default user

