export const fetchJson = async function(file) {
    console.log("file Name", file)
    const res = await fetch(`./content/${file}.json`)
    const data  = await res.json()
    console.log("res", res)
    console.log("data", data)
}

export const fetchTxt = function() {
    console.log("fetchTxt")
}

export const fetchMd = function() {
    console.log("fetchMd")
}

export const fetchJs = function() {
    console.log("fetchJs")
}

export const fetchHtml = function() {
    console.log("fetchHtml")
}