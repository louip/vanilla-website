import {
    fetchJson,
    fetchTxt,
    fetchMd,
    fetchJs,
    fetchHtml
} from "./test-module.js"

const init = function() {
    console.log("test-module.js")
    fetchJson("data")
}

export default init